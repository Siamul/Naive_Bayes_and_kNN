
package naive_bayas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ttest {
    
    double mean1,mean2,s1,s2,doff,n1,n2,d,t;
    double[] sample1,sample2;
    public ttest()
    {
        mean1 = mean2 = s1 = s2 = doff = 0;
        n1 = n2 = 50;
        d = 5;
        sample1 = new double[50];
        sample2 = new double[50];
    }
    
    public void readFile()
    {
        try {
            Scanner in = new Scanner(new File("output.txt"));
            in.nextLine();
            in.nextLine();
                   
            
            for(int i=0;i<100;i++)
            {
                String line = in.nextLine();
                if(i<50)
                {
                    line = line.substring(11);
                    sample1[i] = Double.parseDouble(line);
                }
                else
                {
                    line = line.substring(12);
                    sample2[i-50] = Double.parseDouble(line);
                }
                
                
                //System.out.println(line);
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ttest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void calculate()
    {
        readFile();
        for(int i=0;i<50;i++)
        {
            System.out.println(sample1[i]);
        }
        for(int i=0;i<50;i++)
        {
            System.out.println(sample2[i]);
        }
        double sum1=0,sum2=0;
        for(int i=0;i<sample1.length;i++)
        {
            sum1 += sample1[i];
            sum2 += sample2[i];
        }
        mean1 = sum1/n1;
        mean2 = sum2/n2;
        
        //System.out.println("m1 ="+mean1);
        //System.out.println("m2 = "+mean2);
        sum1 = sum2 = 0;
        for(int i=0;i<sample1.length;i++)
        {
            sum1 += Math.pow((sample1[i]-mean1), 2);
            sum2 += Math.pow((sample2[i]-mean2), 2);
        }
        s1 = sum1/(n1-1);
        s2 = sum2/(n2-1);
        
        //System.out.println("s1 ="+s1);
        //System.out.println("s2 = "+s2);
        
        double f1 = s1/n1;
        double f2 = s2/n2;
        t = Math.round((mean1-mean2-d)/(Math.sqrt(f1+f2))*100.0)/100.0;
        
        doff = Math.pow(f1+f2, 2)/(Math.pow(f1, 2)/(n1-1)+Math.pow(f2, 2)/(n2-1));
        
        int dof = (int)Math.round(doff);
        
        System.out.println("t value = "+t+", d.o.f. = "+dof);
        
    }
    
}
