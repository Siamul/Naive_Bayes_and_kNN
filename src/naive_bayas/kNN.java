/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naive_bayas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 *
 * @author Siamul Karim Khan
 */
public class kNN {
    static int[] IDF;
    //static boolean IDFcalculated = false;
    static HashMap<String, Integer> docFreq = new HashMap<>();
    static int totalDocNo = 0;
   // public static int type = 2; //0 - Hamming Distance, 1 - Euclidean Distance, >=2 - Angle Distance
    static class PQsort implements Comparator<TD> {
        @Override
        public int compare(TD one, TD two) {
            if(one.distance < two.distance)
            {
                return -1;
            }
            else if(one.distance == two.distance)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
    public static String classify(Map<String, Integer> doc, Map<String, Integer>[][] trainCounts, int k, int type)
    {
        //System.out.println(doc);
        double[][] topicDistance = new double[trainCounts.length][k];
        for(int i = 0; i<topicDistance.length; i++)
        {
            for(int j = 0; j<k; j++)
            {
                topicDistance[i][j] = 0;
            }
        }
        double distance = 0;
        PQsort cmp = new PQsort();
        PriorityQueue<TD> pq = new PriorityQueue<>(2000,cmp);
        for(int i = 0; i<trainCounts.length; i++)
        {
            for(int j = 0; j<trainCounts[i].length; j++)
            {
                distance = calcDistance(doc,trainCounts[i][j],type,trainCounts);
                pq.add(new TD(Naive_bayas.topic_arr[i],distance));
            }
        }
        Vector<String> topKNeighbors = new Vector<>();
        for(int i = 0; i<k; i++)
        {
            TD neighbor = pq.remove();
            topKNeighbors.add(neighbor.topicName);   
        }
       // System.out.println(topKNeighbors);
        Map<String, Integer> neighborsNo = new HashMap<>();
        for(String n : topKNeighbors)
        {
            if(neighborsNo.containsKey(n))
            {
                int nNVal = neighborsNo.get(n);
                neighborsNo.put(n, nNVal+1);
            }
            else
            {
                neighborsNo.put(n,1);
            }
        }
        PriorityQueue<TD> pq2 = new PriorityQueue<>(neighborsNo.size(), cmp);
        for(Map.Entry<String, Integer> entry : neighborsNo.entrySet())
        {
            pq2.add(new TD(entry.getKey(), -entry.getValue()));
        }
        return pq2.remove().topicName;
    }
    public static void calculateIDF(Map<String, Integer>[][] totalDocs)
    {
        docFreq.clear();
        //long startTime = System.currentTimeMillis();
        for(int i = 0; i<totalDocs.length; i++)
        {
            totalDocNo += totalDocs[i].length;
            for(int j = 0; j<totalDocs[i].length; j++)
            { 
                for(String key : totalDocs[i][j].keySet())
                {
                    if(docFreq.containsKey(key))
                    {
                        int dfval = docFreq.get(key);
                        docFreq.put(key, dfval+1);
                    }
                    else
                    {
                        docFreq.put(key, 1);
                    }
                }
            }
        }
        //System.out.println("time for IDF = " + (System.currentTimeMillis() - startTime));
    }
    public static double calcDistance(Map<String, Integer> doc1, Map<String, Integer> doc2, int type, Map<String, Integer>[][] totalDocs)
    {
        double distance = 0;
        switch (type) {
        //Hamming Distance
            case 0:
                for (Map.Entry<String, Integer> entry : doc1.entrySet())
                {
                    //System.out.println(entry.getKey() + "/" + entry.getValue());
                    if(!doc2.containsKey(entry.getKey()))
                    {
                        distance++;
                    }
                }
                for (Map.Entry<String, Integer> entry : doc2.entrySet())
                {
                    //System.out.println(entry.getKey() + "/" + entry.getValue());
                    if(!doc1.containsKey(entry.getKey()))
                    {
                        distance++;
                    }
                }
                return distance;
        //Euclidean Distance
            case 1:
                for (Map.Entry<String, Integer> entry : doc1.entrySet())
                {
                    //System.out.println(entry.getKey() + "/" + entry.getValue());
                    int val = entry.getValue();
                    if(doc2.containsKey(entry.getKey()))
                    {
                        val -= doc2.get(entry.getKey());
                    }
                    distance += val*val;
                }
                for (Map.Entry<String, Integer> entry : doc2.entrySet())
                {
                    int val = entry.getValue();
                    //System.out.println(entry.getKey() + "/" + entry.getValue());
                    if(!doc1.containsKey(entry.getKey()))
                    {
                        distance += val*val;
                    }
                }
                return distance;
        //TFxIDF
            default:
                //long startTime = System.currentTimeMillis();
                double fd1 = 0;
                for(int value : doc1.values())
                {
                    fd1 += (double) value;
                }
                double fd2 = 0;
                for(int value : doc2.values())
                {
                    fd2 += (double) value;
                }
               // System.out.println("time for totalFreq = " + (System.currentTimeMillis() - startTime));
                //System.out.print(fd1 + " ");
                //System.out.println(fd2);
                //System.out.println(fd1 + fd2);
                //startTime = System.currentTimeMillis();
                Map<String, Double> TFIDF_D1 = new HashMap<>();
                Map<String, Double> TFIDF_D2 = new HashMap<>();
                double TFIDF_val;
                for(Map.Entry<String, Integer> entry : doc1.entrySet())
                {
                    String dkey = entry.getKey();
                    if(docFreq.containsKey(dkey)) TFIDF_val = ((double)entry.getValue()/(double)fd1) * Math.log((double)totalDocNo/(double)docFreq.get(dkey));
                    else TFIDF_val = ((double)entry.getValue()/(double) fd1) * Math.log((double)totalDocNo);
                    TFIDF_D1.put(entry.getKey(), TFIDF_val);
                }
                for(Map.Entry<String, Integer> entry : doc2.entrySet())
                {
                    String dkey = entry.getKey();
                    if(docFreq.containsKey(dkey)) TFIDF_val = ((double)entry.getValue()/(double)fd2) * Math.log((double)totalDocNo/(double)docFreq.get(dkey));
                    else TFIDF_val = ((double)entry.getValue()/(double) fd2) * Math.log((double)totalDocNo);
                    TFIDF_D2.put(entry.getKey(), TFIDF_val);
                }
                //System.out.println("time for TFIDF = " + (System.currentTimeMillis() - startTime));
                //System.out.println("phase 1");
                double magD1 = 0;
                double magD2 = 0;
                double D1dotD2 = 0;
                for(Map.Entry<String, Double> entry : TFIDF_D1.entrySet())
                {
                    magD1 += entry.getValue() * entry.getValue();
                }
                for(Map.Entry<String, Double> entry : TFIDF_D2.entrySet())
                {
                    magD2 += entry.getValue() * entry.getValue();
                }
                //System.out.println("phase 2");
                Set<String> intersection = new HashSet<>();
                for(String key : TFIDF_D1.keySet())
                {
                    if(TFIDF_D2.containsKey(key))
                    {
                        intersection.add(key);
                    }
                }
                for(String key : intersection)
                {
                    D1dotD2 += TFIDF_D1.get(key) * TFIDF_D2.get(key);
                }
                distance = Math.acos(D1dotD2 / Math.sqrt(magD1 * magD2));
                return Math.abs(distance);
        }
    }
}
