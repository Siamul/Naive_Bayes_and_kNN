/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naive_bayas;

/**
 *
 * @author Siamul Karim Khan
 */
public class TD {
   // public int topicNo;
    public String topicName;
    public int index;
    public double distance;
    TD(String topicName, double distance)
    {
        this.topicName = topicName;
        this.distance = distance;
    }
    public String toString() {
        return "Topic Name: " + topicName + ", Value: " + distance;
    }
}
