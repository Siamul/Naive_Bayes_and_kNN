
package naive_bayas;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Naive_bayas {
    
    static String[] topic_arr;
    static HashSet<String> all;
    static Preprocess_and_Prepare obj[];
    static CalculateProbability object[];
    static double PC;
    static int count=0;
    static HashMap<String, Integer> INeedIt = new HashMap<>();
    static Map<String, Integer>[][] trainSet;
    static double[] sample1, sample2;
    static double max_acc,best_sc, max_acc_kNN;
    static int best_k = 0;
    static int totalNoOfDocs = 0;
    
    public static void Naive_bayas()
    {
        topic_arr = new String[4];
        all = new HashSet<String>();
        obj = new Preprocess_and_Prepare[8];
        object = new CalculateProbability[8];
        sample1 = new double[50];
        sample2 = new double[50];
        max_acc = best_sc = 0;
    }
    
    public CalculateProbability getCalc(int index)
    {
        return object[index];
    }
    
    public Preprocess_and_Prepare getpre(int index)
    {
        return (obj[index]);
    }
    
    public static void readTopicFile()
    {
        Naive_bayas();
        try {
            Scanner in = new Scanner(new File("Data/topics.txt"));
            
            while(in.hasNextLine())
            {
                String[] t = in.nextLine().split(" ");
                topic_arr[count] = t[0].toString();
                INeedIt.put(topic_arr[count],count);
                count++;
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Naive_bayas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static double repeat(int l,int flag)
    {
        
        double sum_acc=0;//Used in else below
        for(int i=0;i<count;i++)
        {
            String tr= topic_arr[i];
            Preprocess_and_Prepare ob = new Preprocess_and_Prepare(tr,l);
            ob.prepare();
            obj[i] = ob;
           
            Set<String> keys = ob.hm.keySet();
            for(String key: keys)
            {
                all.add(key);
            }           
        }
        PC = (double)1/(count);
        //System.out.println(PC);
        int V = all.size();
        trainSet = new Map[count][];
        for(int i = 0; i<count; i++)
        {
            trainSet[i] = new Map[obj[i].docArray.size()];
            trainSet[i] = obj[i].docArray.toArray(trainSet[i]);
            totalNoOfDocs += trainSet[i].length;
        }
        if(flag == 1 || flag == 3) kNN.calculateIDF(trainSet);
        //System.out.println(totalNoOfDocs);
        //int counter = 0;
        

        all.clear();
        
        if(flag == 1)//then do best sc choosing works ,,else dont
        {
            int counter = 0;
            double sc = 0,acc=0;
            for(int j =1;j<=50;j++)
            {
                sc += 0.04;
                acc = 0;
                counter=0;
                for(int i=0;i<count;i++)
                {
                    String tr= topic_arr[i];
                    CalculateProbability p = new CalculateProbability(tr,counter,V,PC,count,sc);

                   p.calculate();
                   acc += p.correct;
                   object[counter++] = p;
                }
                acc = acc*100/200;
                if(acc >max_acc)
                {
                    max_acc = acc;
                    best_sc = sc;
                }
            }
            counter = 0;
            max_acc_kNN = 0;
            for(int j =1;j<=50;j++)
            {
                acc = 0;
                //counter=0;
                for(int i=0;i<count;i++)
                {
                    String tr= topic_arr[i];
                    CalculateProbability p = new CalculateProbability(tr,j);

                   p.calculateKNN();
                   acc += p.correct;
                   //object[counter++] = p;
                }
                acc = acc*100/200;
                //System.out.println("accuracy = " + acc);
                if(acc > max_acc_kNN)
                {
                    max_acc_kNN = acc;
                    best_k = j;
                }
            }      
        }
        if(flag == 2)//now with that best sc call preprocess for every 100 linees
        {
           for(int i=0;i<count;i++)
            {
                String tr= topic_arr[i];
                CalculateProbability p = new CalculateProbability(tr,best_k);

                p.calculateKNN();
                sum_acc += p.correct;
               //object[counter++] = p;
            }
            sample2[l/100] = sum_acc*100/200;
            int counter=0;
            sum_acc = 0;
            for(int i=0;i<count;i++)
            {
                String tr= topic_arr[i];
                CalculateProbability p = new CalculateProbability(tr,counter,V,PC,count,best_sc);

               p.calculate();
               sum_acc += p.correct;
               object[counter++] = p;
            }
            sum_acc = sum_acc*100/200;
        }
        return sum_acc;
    }
    
    public static void main(String[] args) 
    {
        
        readTopicFile();
        //System.out.println(count);
        
        repeat(-1,1);
        System.out.println("NB best Acc = "+max_acc+", best sc = "+best_sc);
        System.out.println("kNN best Acc = "+ max_acc_kNN + ", best_k = " + best_k);
        int l=0;
        for(int i=0;i<50;i++)
        {
            sample1[i] = repeat(l,2);
            l += 100;
            
        }
        for(int i=0;i<50;i++)
        {
            System.out.println(i+".NB Acc = "+sample1[i]);
            
        }
                for(int i=0;i<50;i++)
        {
            System.out.println(i+".kNN Acc = "+sample2[i]);
            
        }
        //==================================Upto here============================== 
        
        //==================From here your code..erased file writtings,,else didnt do anything==================
               
    }
  }
