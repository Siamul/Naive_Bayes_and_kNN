
package naive_bayas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CalculateProbability {
    
    HashMap<String, Integer> train ;
    ArrayList<String> test ;
    String topic_name;
    String file_name;
    Preprocess_and_Prepare pre;
    double probability;
    int index;//which test topic
    int V,NC,total_topics;
    double PC;
    double smoothing_constant;
    double accuracy,correct;
    double [] prob;
    int[] NCW;
    int k = 0;
    
    public CalculateProbability(String t,int index,int V,double PC,int topics,double sc)
    {
        train = new HashMap<String,Integer>();
        test = new ArrayList<String>();
        topic_name = t;
        file_name = "Data/Test/"+topic_name+".xml";
        pre = new Preprocess_and_Prepare("",-1);
        this.index = index;
        this.V = V;
        this.PC = PC;
        total_topics = topics;
        prob = new double[topics];
        smoothing_constant = sc;
        NCW = new int[topics];
        correct =0;
    }
    
    public CalculateProbability(String t, int k)
    {
        topic_name = t;
        file_name = "Data/Test/"+topic_name+".xml";
        pre = new Preprocess_and_Prepare("",-1);
        this.k = k;
        
    }
    
    public void readExceptions()
    {
        try {
            Scanner r = new Scanner(new File("exc.txt"));
            while(r.hasNext())
            {
                pre.hs.add(r.nextLine().toLowerCase());
            }
            //System.out.println(hs.size());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Preprocess_and_Prepare.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void calculateKNN()
    {
        pre.readExceptions();
        try {
            Scanner in = new Scanner(new File(file_name));
            int counter = 0;
            while(in.hasNextLine())
            {
                if(counter == 50)break;

                String line = in.nextLine();
                if(line.startsWith("<row",2))
                {
                    //System.out.println("Reading line"+counter+"------------------------------->>>>>>>>>>>>>>>>>>>>>>>");
                    Map<String, Integer> docMap = pre.preProcess(line);
                    String classification = kNN.classify(docMap, Naive_bayas.trainSet, k, 2);
                    if(classification.compareTo(topic_name) == 0) correct++;
                    counter++;
                }
            }
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }
    
    public void calculate()
    {
        Naive_bayas nv = new Naive_bayas();
        
        pre.readExceptions();
        //System.out.println(pre.hs.size());
        try {
            Scanner in = new Scanner(new File(file_name));
            int counter = 0;
         
            while(in.hasNextLine())
            {
                if(counter == 50)break;

                String line = in.nextLine();
                if(line.startsWith("<row",2))
                {
                    //System.out.println("Reading line"+counter+"------------------------------->>>>>>>>>>>>>>>>>>>>>>>");
                    pre.preProcess(line);

                    Set<String> keys = pre.hm.keySet();
                    for(String key: keys)
                    {
                        if(pre.hm.get(key)== 1)test.add(key);
                        else
                        {
                            for(int v=0;v<pre.hm.get(key);v++)
                            {
                                test.add(key);
                            }
                        }
                    }

                    //Do some calculations

                    Iterator<String> itr = test.iterator();
                    for(int i=0;i<total_topics;i++)
                    {
                        probability = 0;
                        Preprocess_and_Prepare p = new Preprocess_and_Prepare("",-1);
                        p = nv.getpre(i);
                        NC=0;
                        if(counter == 0)
                        {
                            Set<String> nk = p.hm.keySet();
                            for(String n: nk)
                            {
                                NC += p.hm.get(n);
                            }
                            NCW[i] = NC;
                        }
                        else NC = NCW[i];
                        train.putAll(p.hm);
                       //System.out.println(p.topic_name+", NC = "+NC);
                        
                        probability += Math.log10(PC);
                        while(itr.hasNext())
                        {
                            int val = 0;
                            String testStr = itr.next();
                            if(train.containsKey(testStr))
                            {
                               // System.out.println("Yes, Test data +"+index+"contains = "+testStr);
                                val = train.get(testStr);
                            }
                            else
                            {
                                val = 0;
                            }
                            
                            probability += Math.log10((val + smoothing_constant)/(NC+V*smoothing_constant));
                            //System.out.println("lnm = "+lnm+"den = "+den+"NC = "+NC+" V = "+V);
                        }
                        prob[i] = probability;
                        
                        train.clear();
                        itr = test.iterator();
                    }
                    
                    int bestindex=0;
                    double max = prob[0];
                    
                    for(int i=1;i<total_topics;i++)
                    {
                        if(prob[i] > max)
                        {
                            max = prob[i];
                            bestindex = i;
                        }
                    }
                    //System.out.println("predicted = "+bestindex+",index =  " +index);
                    if(bestindex == index)correct++;
                    counter++;
                    pre.hm.clear();
                    test.clear();
                }
                //System.out.println(correct);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Naive_bayas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}