package naive_bayas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Preprocess_and_Prepare {
    
    
    HashMap<String, Integer> hm ;
    ArrayList<Map<String, Integer> > docArray = new ArrayList<>();
    HashSet<String> hs ;
    String topic_name;
    String file_name;
    int hash_size=0;
    int docIndex = 0;
    int linecount,limit;
   
    public Preprocess_and_Prepare(String t,int l)
    {
        hm = new HashMap<String,Integer>();
        hs = new HashSet<String>();
        topic_name = t;
        file_name = "Data/Training/"+topic_name+".xml";
        if(l == -1)
        {
            limit = 500;
            linecount = 0;
        }
        else
        {
            linecount=l;
            limit = 100;
        }
        docArray.clear();
    }
    
    public void readExceptions()
    {
        try {
            Scanner r = new Scanner(new File("exc.txt"));
            while(r.hasNext())
            {
                hs.add(r.nextLine().toLowerCase());
            }
            //System.out.println(hs.size());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Preprocess_and_Prepare.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public Map<String,Integer> preProcess(String line)
    {
        //line = line.toLowerCase();
        int sindex = line.indexOf("Body=",0);
        int eindex = line.indexOf("OwnerUserId=");
        //System.out.println(eindex);
        if(eindex == -1)
        {
            eindex = line.indexOf("OwnerDisplayName=");
        }
        if(eindex == -1)
        {
            eindex = line.indexOf("LastEditor");
        }
        if(eindex == -1)
        {
            eindex = line.indexOf("LastActivity");
        }
         //System.out.println(line);
        //if(limit == 100)System.out.println("sind = "+sindex+",einde = "+eindex);
        //if(eindex == -1 ) System.out.println(line);
        if(eindex != -1 && sindex != -1) line = line.substring(sindex+6,eindex-2);
        else if(eindex == -1 && sindex != -1) line.substring(sindex+6);
        else if(sindex == -1 && eindex != -1) line.substring(0, eindex-2); 
        
        line = line.replace("&quot", "");
       
        int hindex = line.indexOf("href");
        //System.out.println("hindex = "+hindex+",hindex2="+hindex2);
        int qindex;
        while(hindex != -1)
        {
            qindex = line.indexOf("&gt", hindex+10);
            //System.out.println(line);
            //System.out.println(line.substring(0,hindex-1).concat(line.substring(qindex+1)));
            line = line.substring(0,hindex-1).concat(line.substring(qindex+1));
            hindex = line.indexOf("href");
           // System.out.println(hindex);
        }
        
        hindex = line.indexOf("http");
        if(hindex != -1)
        {
            qindex = line.indexOf("&gt", hindex+10);
            line = line.substring(0,hindex-1)+line.substring(qindex+1);
        }
        line = line.replaceAll("[\\p{P}&&[^\\u0027]]", "").replaceAll("[\\d]", "").replaceAll("\\s{2,}", " ");
        
        String[] token = line.split(" ");
        for(String tk:token)
        {
            int flag = 0;
            if(hs.contains(tk.toLowerCase()))flag =1;
            if(tk.length() != 1 && flag ==0 && !tk.equals(""))
            {
                if(hm.containsKey(tk))
                {
                    int val = hm.get(tk);
                    val++;
                    hm.put(tk, val);
                }
                else hm.put(tk.toLowerCase(), 1);
                
                //System.out.println(tk);
            }
        }
        Map<String, Integer> docMap = new HashMap<>();
        for(int i = 0; i < token.length; i++)
        {
            if(docMap.containsKey(token[i]))
            {
                int docVal = docMap.get(token[i]);
                docMap.put(token[i],docVal+1);
            }
            else
            {
                docMap.put(token[i], 1);
            }
        }
        docArray.add(docMap);   
        return docMap;
        
    }
    
    public void prepare()
    {
        readExceptions();
        try {
            Scanner in = new Scanner(new File(file_name));
            int counter = 0;
          
            int i=0;
            while(i<linecount)
            {
                String line = in.nextLine();
                if(line.startsWith("<row",2))
                {
                    i++;
                }
            }
            while(in.hasNextLine())
            {
                if(counter == limit)break;
                
                String line = in.nextLine();
                if(line.startsWith("<row",2))
                {
                    //System.out.println("Reading Line"+counter+"------------------------>>>>>>>>>>>>>>>>"+line.toCharArray()[11]);
                    preProcess(line);
                    counter++;
                }
            }
            //System.out.println("DONE FOR this 100 =====================>>>>>>>>>>>>>>>>>>>>>>>>>>");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Naive_bayas.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        hash_size = hm.size();
    }   
}